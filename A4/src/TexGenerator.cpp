#include "AttributedObject.h"
#include <stdio.h>
#include <math.h>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

AttributedObject* obj;
// Texture indexed as [x=0...1023][y=0...1023]
Cartesian3 texture[1024][1024];
Cartesian3 normalMap[1024][1024];

struct vertex {
    Cartesian3 tex, col, norm;
};

struct triangle {
    vertex v1,v2,v3;
};

// Forward declerations
void generateTex();
void writeTexPPM(const char* filename);
void writeNormPPM(const char* filename);
void printTriangle(triangle* tri);
int mapTo255(float val, float minIn, float maxIn);
float dot(Cartesian3 a, Cartesian3 b);

int main(int argc, char const *argv[])
{
    // Validate command line input
    if (argc != 2) {
        cout << "Usage: ./TexGen [obj file]" << endl;
        return 0;
    }
    
    ifstream f(argv[1]);
    if (!f) {
        cout << "Error opening file" << endl;
        return 0;
    }

    // Set texture to black
    for (size_t x = 0; x < 1024; x++)
        for (size_t y = 0; y < 1024; y++)
        {
            texture[x][y] = Cartesian3(0.f,0.f,0.f);
            normalMap[x][y] = Cartesian3(0.f,0.f,0.f);
        }    

    // Read obj file
    obj = new AttributedObject();

    obj->ReadObjectStream(f);

    f.close();

    // Generate the texture and normal map
    generateTex();

    // Generate output file name
    string OutFilename = string(argv[1]).substr(0,string(argv[1]).length()-4);

    // Generate the output files
    writeTexPPM((OutFilename + "_texture.ppm").c_str());
    writeNormPPM((OutFilename + "_normals.ppm").c_str());

    delete obj;

    return 0;
}


void generateTex() {
    // For each face (assumes face size of 3 verts)
    for (size_t face = 0; face < obj->faceVertices.size(); face+=3)
        { // per face
            vertex v1;
            // Data for the first vertex
            v1.tex = obj->textureCoords[obj->faceTexCoords[face]];
            v1.col = obj->colours[obj->faceColours[face]];
            v1.norm = obj->normals[obj->faceNormals[face]];

            vertex v2;
            // Data for the second vertex
            v2.tex = obj->textureCoords[obj->faceTexCoords[face+1]];
            v2.col = obj->colours[obj->faceColours[face+1]];
            v2.norm = obj->normals[obj->faceNormals[face+1]];

            vertex v3;
            // Data for the third vertex
            v3.tex = obj->textureCoords[obj->faceTexCoords[face+2]];
            v3.col = obj->colours[obj->faceColours[face+2]];
            v3.norm = obj->normals[obj->faceNormals[face+2]];

            triangle tri{v1,v2,v3};
            // Print the triangle to the texture and normal map array
            printTriangle(&tri);
        } // per face
}

// Print a triangle to the texture and normal arrays
void printTriangle(triangle* tri) {
    // Convert tex coords from 0-1 to 0-1023
    Cartesian3 a = tri->v1.tex * 1023;
    Cartesian3 b = tri->v2.tex * 1023;
    Cartesian3 c = tri->v3.tex * 1023;

    // Find the bounding box of the triangle
    float minx = min(min(a.x,b.x),c.x);
    float maxx = max(max(a.x,b.x),c.x);
    float miny = min(min(a.y,b.y),c.y);
    float maxy = max(max(a.y,b.y),c.y);

    // Precalculate the point independent values in calculating the barycentric coords of a point
    // Barycentric coordinates are calculated using the method outlined by Christer Ericson in "Real-Time Collision Detection"
    Cartesian3 v0 = b - a, v1 = c - a;
    float dot00 = dot(v0,v0);
    float dot01 = dot(v0,v1);
    float dot11 = dot(v1,v1);
    float denominator = (dot00 * dot11) - (dot01 * dot01);

    // For each pixel in the bounding box
    for (size_t px = floor(minx); px <= ceil(maxx); px++)
        for (size_t py = floor(miny); py <= ceil(maxy); py++)
        {
            // Calculate the alpha, beta and gamma of point p in the triangle
            Cartesian3 v2 = Cartesian3(px,py,0) - a;
            float dot20 = dot(v2,v0);
            float dot21 = dot(v2,v1);

            float beta = ((dot11*dot20)-(dot01*dot21))/denominator;
            float gamma  = ((dot00*dot21)-(dot01*dot20))/denominator;
            float alpha = 1.0f - beta - gamma;

            // If the point is in the triangle
            // No need to check if they sum to <= 1 as this is ensured by the definition of alpha
            // If the "true" sum is > 1 then alpha will be < 0 and will still be detected as outside the triangle
            if (alpha >= 0 and beta >= 0 and gamma >= 0) {
                Cartesian3 colour = (tri->v1.col*alpha) + (tri->v2.col*beta) + (tri->v3.col*gamma);
                Cartesian3 normal = (tri->v1.norm*alpha) + (tri->v2.norm*beta) + (tri->v3.norm*gamma);
                texture[px][py] = colour;
                normalMap[px][py] = normal;
            }
        }
}

// Save the contents of the texture array to a ppm file
void writeTexPPM(const char* filename) {
    ofstream f(filename);
    if (!f) cout << "Error writing to file" << endl;

    // PPM header
    f << "P3" << endl << "1024" << endl << "1024" << endl << "255" << endl;
    // For each pixel in the image/array
    for (size_t x = 0; x < 1024; x++) {
        for (size_t y = 0; y < 1024; y++)
        {
            Cartesian3 t = texture[x][y];
            // Write the color value to the ppm file
            f << mapTo255(t.x, 0.f, 1.f) << " " 
              << mapTo255(t.y, 0.f, 1.f) << " "
              << mapTo255(t.z, 0.f, 1.f) << endl;
        }
    }
    f.close();
}

// Identical to above but for the normal map array
void writeNormPPM(const char* filename) {
    ofstream f(filename);
    if (!f) cout << "Error writing to file" << endl;

    f << "P3" << endl << "1024" << endl << "1024" << endl << "255" << endl;
    for (size_t x = 0; x < 1024; x++) {

        for (size_t y = 0; y < 1024; y++)
        {
            Cartesian3 n = normalMap[x][y];
            f << mapTo255(n.x, -1.f, 1.f) << " " 
              << mapTo255(n.y, -1.f, 1.f) << " "
              << mapTo255(n.z, -1.f, 1.f) << endl;
        }
    }
    f.close();
}

// Map a value from the range [minIn, maxIn] to the range [0,255] (a valid R,G, or B value)
int mapTo255(float val, float minIn, float maxIn) {
    float top = val - minIn;
    float bot = maxIn - minIn;
    float outVal = (top/bot)*255.f;
    return (int)outVal;
}

// 2D dot product, assuming z=0
float dot (Cartesian3 a, Cartesian3 b) {
    return (a.x*b.x) + (a.y*b.y);
}