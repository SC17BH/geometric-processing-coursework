#include <iostream>
#include <fstream>
#include <algorithm>
#include <exception>
#include "face2faceindex.h"

using namespace std;

int main (int argc, char *argv[]) {
    // Ensure the command line argument(s) are valid
    if (argc != 2) {
        cout<<"Please supply one argument: \n1: The .tri file to be converted\n";
        return 0;
    }
    string fileInName = argv[1];
    if (fileInName.substr(fileInName.length()-4) != ".tri") {
        cout << "Please supply an input argument with the .tri file extension\n";
        return 0;
    }

    cout << "File to be converted: " << fileInName << "\n";

    // readTriFile will return 0 if the file could not be opened, in which case quit the program
    if (!readTriFile(fileInName)) {
        return 0;
    }

    // If parseFile throws an invalid_argument exception it means that one of the 'words' in the file is not a number
    try
    {
        parseFile();
    }
    catch(exception& e)
    {
        cout << "The file supplied is not a valid .tri file" << endl;
        std::cerr << e.what() << '\n';
    }
    writeToFile(fileInName);
}

void parseFile() {
    faceCount = stoi(fileContents[0]);

    // For each face in the file
    for (int i = 0; i < faceCount; i++)
    {
        // The index in the fileContents vector that the face starts at
        int faceIndex = (9*i) + 1;
        int v1 = getVertexIndex(faceIndex);
        int v2 = getVertexIndex(faceIndex + 3);
        int v3 = getVertexIndex(faceIndex + 6);

        faces.push_back(TriFace{v1,v2,v3});
    }
}

int getVertexIndex(int fcIndex) {
    // Find the coordinates of the vertex
    float a = stof(fileContents[fcIndex]);
    float b = stof(fileContents[fcIndex+1]);
    float c = stof(fileContents[fcIndex+2]);

    // Define the vertex struct instance
    Vert3f v{a,b,c};

    // Check the working vertex against all currently tracked vertices
    for (size_t i = 0; i < verts.size(); i++)
    {
        if (equalVertices(verts[i],v)) {
            return i;
        }
    }
    verts.push_back(Vert3f{a,b,c});
    return (verts.size() -1);

}

// Function to read from the supplied file to the fileContents String
// Returns 1 if sucessful, 0 otherwise
int readTriFile (std::string fileName) {
    // open the file with the supplied file name
    ifstream file(fileName);

    // If the file is oppened succesfully
    if (file.is_open()){  
        // read file word by word to a temp string and place into fileContents
        // Each element of fileContents will be one word from the file  
        string token;    
        while (file >> token) {
            fileContents.push_back(token.c_str());
        }
        file.close();

    } else {
        cout<<fileName<<": file not found\n";
        return 0;
    }
    return 1;
}

bool equalVertices(Vert3f a, Vert3f b) {
    return (a.x == b.x and a.y == b.y and a.z == b.z);
}

void writeToFile (std::string fileName) {

    string fileOutName = fileName.substr(0,fileName.length()-4);

    ofstream outFile(fileOutName + ".face");

    if (outFile.is_open()) {
        outFile << "# University of Leeds 2020-2021\n"
                    "# COMP 5821M Assignment 1\n"
                    "# Ben Hulse\n"
                    "# 201125516\n"
                    "#\n"
                    "# Object Name: " << fileOutName.substr(fileOutName.find_last_of('/')+1)<<"\n"
                    "# Vertices="<<verts.size()<<"\n# Faces="<<faceCount<<"\n#\n";


        for (size_t i = 0; i < verts.size(); i++)
        {
            outFile << "Vertex " << i << " " << verts[i].x << " " << verts[i].y << " " << verts[i].z << std::endl;
        }
        for (int i = 0; i < faceCount; i++) {
            outFile << "Face " << i << " " << faces[i].v1 << " " << faces[i].v2 << " " << faces[i].v3 << std::endl;
        }
        std::cout << "Output to file: "<< fileOutName + ".face"<<"\n";
        outFile.close();
    } else {
        std::cout<< "File could not be opened\n";
    }
}