University of Leeds 2020-2021
COMP 5821M Assignment 1
Ben Hulse
201125516

# Compiling the code

To compile the code, inside the 'source/' folder run the makefile with either: the argument 'all' to compile both files, or just the name of the file to be compiled ('face2faceindex'/'faceindex2directededge').

# Running the program

To run the program, run the excecutable with a single command line argument: the file path of the file to be compiled.

For 'face2faceindex' the file supplied must be a '.tri' file type. The output will be a file of the same name, in the same directory, with the file extension '.face'. 

For 'faceindex2directededge' the file supplied must be a '.face' file type. The output will be a file of the same name, in the same directory, with the file extension '.diredge'. 
This program will also print to the console if the shape supplied is not manifold. If the shape is manifold it will print to console the genus of the shape.

# Complexity analysis

face2faceindex:
The complexity of face2faceindex is O(n^2) as when adding a new face, it will loop over the vertex array three times for every face to check if each vertex has been added already. This would make O(fv) where v is the vertex count and f is the face count, or O(n^2).

faceindex2directededge:
The complexity of faceindex2directededge is O(n^2) as in multiple places it has 2 nested loops that go over some part of the input (vertices/faces). 
The countShapes() function is O(n^2) as for every vertex, it will make a recursive call over the vertices. However this recursive call can only be called once per vertex and so is capped at O(v) where v is the vertex count. So the complexity of countShapes() is O(v^2) or O(n^2).
