///////////////////////////////////////////////////
//
//	Hamish Carr
//	February, 2019
//
//	------------------------
//	GeometricController.cpp
//	------------------------
//	
///////////////////////////////////////////////////

#include "GeometricController.h"

// constructor
GeometricController::GeometricController(GeometricSurfaceDirectedEdge *Surface, GeometricWindow *Window)
	: surface(Surface), window(Window)
	{ // constructor
	QObject::connect(	window->smoothnessSlider,	SIGNAL(valueChanged(int)), 
						this,						SLOT(smoothnessSliderChanged(int)));
	} // constructor

// slot for responding to slider changes
void GeometricController::smoothnessSliderChanged(int value)
	{ // smoothnessSliderChanged()
	printf("New Slider Value: %d\n", value);
	} // smoothnessSliderChanged()
