#include "DirEdge.h"
#include <iostream>
#include <fstream>
#include <stdexcept>

using namespace std;

DirEdge::DirEdge() {

}

// Take in a mesh that has undergone greedy decimation and rebuild
void DirEdge::RebuildDecimated(DirEdge *old) {
    vector<int> viLookup(old->verts.size(),-1);

    // Add verts and make lookup array for new vert indices
    int skips = 0;
    for (size_t vi = 0; vi < old->verts.size(); vi++)
    {
        Vertex3f v = old->verts[vi];

        if (v.deleted) {
            skips ++;
            continue;
        }
        viLookup[vi] = vi - skips;
        verts.push_back(v);
        norms.push_back(old->norms[vi]);
    }

    vCount = verts.size();
    firstDE.assign(vCount,-1);
    
    for (size_t fi = 0; fi < old->faces.size(); fi++)
    {
        Face3i f = old->faces[fi];

        if (f.delted) {
            continue;
        }

        // update face vertices
        f.v1 = viLookup[f.v1];
        f.v2 = viLookup[f.v2];
        f.v3 = viLookup[f.v3];

        // Add face
        faces.push_back(f);

        // Add half edges
        GenHEdge(f.v1,f.v2);
        GenHEdge(f.v2,f.v3);
        GenHEdge(f.v3,f.v1);
    }

    fCount = faces.size();
    eCount = hEdges.size();
    
    // cout << "Lookup" <<endl;
    // for (size_t i = 0; i < viLookup.size(); i++)
    // {
    //     cout << i << "=" << viLookup[i] << endl;
    // }
    

}

void DirEdge::GenHEdge(size_t v1, size_t v2) {
    // Set First Directed Edge if needed
    if (firstDE[v1] == -1) firstDE[v1] = hEdges.size();

    // Get index of new half edge
    size_t hi = hEdges.size();

    // Add half edge to the vector
    hEdges.push_back(HalfEdge2i{v1,v2});

    // Attempt to find the other half to the half edge
    size_t oH = -1;

    for (size_t i = 0; i < hEdges.size(); i++)
    {
        if (hEdges[i] == HalfEdge2i{v2,v1}) {
            oH = i;
            otherH[i] = hi;
        }
    }
    
    otherH.push_back(oH);

}


// Parse the contents of the fileData vector
bool DirEdge::parseFile(vector<string> fileData) {
    size_t currToken = 0;

    // -------- Find Vertex and Face Count --------

    // Go to the index of the vert count line
    for (;fileData[currToken++] != "Surface";);
    
    // Convert the string to int
    vCount = stoi(fileData[currToken++].substr(9));
    fCount = stoi(fileData[currToken++].substr(6));
    eCount = 3*fCount;

    // -------- Parse Vertices/Normals/FDE --------

    // Go to the first instance of "Vertex"
    for (;fileData[currToken] != "Vertex";currToken++);

    // Parse each vertex and push it to the vector
    for (size_t i = 0; i < vCount; i++)
    {
        currToken+=2;
        float x,y,z;

        try
        {
            x = stof(fileData[currToken++]);
            y = stof(fileData[currToken++]);
            z = stof(fileData[currToken++]);
        }
        catch(const invalid_argument& e)
        {
            cout << "Error: File not properly formatted" << endl;
        }

        verts.push_back(Vertex3f(x,y,z));
    }

    // Parse each normal and push it to the vector
    for (size_t i = 0; i < vCount; i++)
    {
        currToken+=2;
        float x,y,z;

        try
        {
            x = stof(fileData[currToken++]);
            y = stof(fileData[currToken++]);
            z = stof(fileData[currToken++]);
        }
        catch(const invalid_argument& e)
        {
            cout << "Error: File not properly formatted" << endl;
        }

        norms.push_back(Vertex3f(x,y,z));
    }

    // Parse each FDE and push it to the vector
    for (size_t i = 0; i < vCount; i++)
    {
        currToken+=2;
        float x,y,z;
        try
        {
            firstDE.push_back((size_t)stoi(fileData[currToken++]));
        }
        catch(const invalid_argument& e)
        {
            cout << "Error: File not properly formatted" << endl;
        }
    }

    // --------------- Faces/Edges ---------------

    // Parse each face and push it to the vector
    // Each face has 3 edges
    for (size_t i = 0; i < fCount; i++)
    {
        currToken+=2;
        size_t v1,v2,v3;

        try
        {
            v1 = (size_t)stoi(fileData[currToken++]);
            v2 = (size_t)stoi(fileData[currToken++]);
            v3 = (size_t)stoi(fileData[currToken++]);
        }
        catch(const invalid_argument& e)
        {
            cout << "Error: File not properly formatted" << endl;
        }
        faces.push_back(Face3i(v1,v2,v3));

        // Files are defined in edge-to form
        hEdges.push_back(HalfEdge2i{v3,v1});
        hEdges.push_back(HalfEdge2i{v1,v2});
        hEdges.push_back(HalfEdge2i{v2,v3});
    }

    // Parse other Halves
    for (size_t i = 0; i < eCount; i++)
    {
        currToken+=2;

        try
        {
            otherH.push_back((size_t)stoi(fileData[currToken++]));
        }
        catch(const invalid_argument& e)
        {
            cout << "Error: File not properly formatted" << endl;
        }

    }

    return true;
}


// Print mesh to console
void DirEdge::printMesh() {
    for (size_t i = 0; i < verts.size(); i++)
    {
        cout << "Vertex " << i << ":" << endl;
        cout << "   Position: " << verts[i] << endl;
        cout << "   Normal: " << norms[i] << endl;
        cout << "   FDE: " << firstDE[i] <<endl;
    }   

    for (size_t i = 0; i < faces.size(); i++)
    {
        cout << "Face " << i << ":" << endl;
        cout << "   Corners: " << faces[i].v1 << " " << faces[i].v2 << " " << faces[i].v3 << endl;
    }

    for (size_t i = 0; i < hEdges.size(); i++)
    {
        cout << "Half Edge " << i << ":" << endl;
        cout << "   Ends: " << hEdges[i].v1 << " " << hEdges[i].v2 << endl;
        cout << "   Other Half: " << otherH[i] << endl;
    }
}

// Write mesh to file
void DirEdge::writeMesh(string fileName) {
    ofstream file (fileName.c_str());

    // Open File
    if (!file.is_open()) {
        cout << "Error: Cannot open file to save" << endl;
        return;
    }

    // Write Header to file
    file << "#" << endl << "# Created for Leeds COMP 5821M Autumn 2020" << endl << "#" << endl << "#" << endl;
    file << "# Surface vertices=" << verts.size() << " faces=" << faces.size() << endl << "#" << endl;

    // Write Vertices
    for (size_t i = 0; i < verts.size(); i++)
    {
        file << "Vertex " << i << " " << verts[i].x << " " << verts[i].y << " " << verts[i].z << endl;
    }
    // Write Normals
    for (size_t i = 0; i < verts.size(); i++)
    {
        file << "Normal " << i << " " << norms[i].x << " " << norms[i].y << " " << norms[i].z << endl;
    }
    // Write FDEs
    for (size_t i = 0; i < firstDE.size(); i++)
    {
        file << "FirstDirectedEdge " << i << " " << firstDE[i]<<endl;
    }
    // Write Faces
    for (size_t i = 0; i < faces.size(); i++)
    {
        file << "Face " << i << " " << faces[i].v1 << " " << faces[i].v2 << " " << faces[i].v3 << endl;
    }
    // Write Other Halves
    for (size_t i = 0; i < otherH.size(); i++)
    {
        file << "OtherHalf " << i << " " << otherH[i] << endl;
    }
    file.close();
}