#include <string>
#include <vector>
#include <queue>
#include "DirEdge.h"

using namespace std;


class Simplifier
{
    // A pair type to be used in the priority queue
    // .first - Curvature of vertex
    // .second - Index of vertex
    typedef pair<float,int> qPair;

private:
    vector<string> fileData;

    DirEdge fullMesh;
    vector<DirEdge> splitMesh;

    // Connected splitting data
    vector<bool> visited;
    // For each vertex in the full mesh, store a pointer to the sub mesh it belongs to
    vector<int> vertDest;

    // greedy decimation variables
    // Priority queue in smallest
    priority_queue<qPair> pQueue;
    // Verts in the oring of each vert
    // vector<vector<int>> oRingv;
    // Faces in the oring of each vert
    vector<vector<int>> oRingf;

public:
    Simplifier();
    bool loadMesh(string fileName);
    void printMesh();
    void writeMesh(string fileName);
    void writeSplitMesh(string filename);

    // Separation
    void separate();
    void visitVert(size_t vIndex, DirEdge *mesh);
    void assignEdgesandFaces();

    // Simplification
    void simplify(int vRemove);
    void getORings();
    void fillPQueue();
    float vertArea(int i);
    float vertCurve(int i);
    void greedyDecimation();
    float triArea(Vertex3f a, Vertex3f b, Vertex3f c);
};