#pragma once

#include <math.h>
#include <iostream>

struct Vector3f {
    float x,y,z;
    
    Vector3f()
    {
        x = 0; y = 0; z = 0;
    }

    Vector3f(float _x, float _y, float _z)
    {
        x = _x; y = _y; z = _z;
    }

    void Normalise() {
        *this /= Length();
    }

    float Length() {
        return sqrt(x*x + y*y + z*z);
    }

    float Length2() {
        return x*x + y*y + z*z;
    }

    Vector3f operator +(Vector3f other) {
        return Vector3f(x+other.x, y+other.y,z+other.z);
    }
    
    Vector3f operator -(Vector3f other) {
        return Vector3f(x-other.x, y-other.y,z-other.z);
    }

    void operator +=(Vector3f other) {
        x += other.x;
        y += other.y;
        z += other.z;
    }

    Vector3f operator /(float divisor) {
        return Vector3f(x/divisor,y/divisor,z/divisor);
    }

    void operator /=(float divisor) {
        x /= divisor;
        y /= divisor;
        z /= divisor;
    }
};

inline std::ostream& operator<<(std::ostream& os, const Vector3f & v) {
    os << "(" << v.x << ", " << v.y << ", " << v.z << ")";
    return os;
}