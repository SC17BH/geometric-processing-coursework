#include <string>
#include <vector>
#include "Vector3.h"

using namespace std;

struct Face3i {
    size_t v1,v2,v3;
};

struct HalfEdge2i {
    size_t v1,v2;
};

class SubDivider
{
private:
    vector<string> fileData;

    // Vertex Data
    vector<Vector3f> verts;
    vector<Vector3f> norms;
    vector<int> firstDE;
    size_t ogVCount;

    // Face Data
    vector<Face3i> faces;
    size_t ogFCount;

    // Original Edge Data
    vector<HalfEdge2i> hEdges;
    vector<size_t> edgeID;
    vector<int> otherH;
    size_t ogECount;

    vector<HalfEdge2i> newHEdges;
    vector<int> newOtherH;
    vector<size_t> fHalf;
    vector<size_t> sHalf;

    bool parseMesh();
    void splitEdges();
    void updateFaces();
    void updateOH();
public:
    SubDivider();
    bool loadMesh(string fileName);
    void printMesh();
    void writeMesh(string fileName);
    void divide();
};