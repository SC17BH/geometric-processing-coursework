#include <iostream>
#include <string>
#include "SubDivider.h"

int main(int argc, char const *argv[])
{
    SubDivider s;

    if (argc != 2) {
        std::cout << "Usage: ./subdividemesh [path to .diredgenormal file]" << endl;
        return 0;
    }

    std::string file = argv[1];

    if (file.substr(file.length() - 14) != ".diredgenormal") {
        std::cout << "Ensure the file has the type .diredgenormal" << endl;
        return 0;
    }
    
    s.loadMesh(file);
    s.divide();

    // s.printMesh();

    string outFile = file.substr(0,file.length() - 14) + "_split.diredgenormal";
    s.writeMesh(outFile);
    return 0;
}